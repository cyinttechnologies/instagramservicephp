<?php
/*InstagramService.php
Symfony Service to connect to Instagram public API
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


namespace CYINT\ComponentsPHP\Services;
class InstagramService
{
    private $Doctrine;
    private $Repository;
    private $client_id;
    private $client_secret;
    private $CurlService;
    private $api_url;
    private $base_access_token;
    private $user_access_token;
    private $redirect_url;

    public function __construct($Doctrine, $CurlService)
    {
        $this->Doctrine = $Doctrine;
        $this->Repository = $Doctrine->getRepository('CYINTSettingsBundle:Setting');
        $instagram_settings = $this->Repository->findByNamespace('instagram');
        $this->setClientSecret($instagram_settings['client_secret']);
        $this->setClientId($instagram_settings['client_id']);
        $this->setApiUrl($instagram_settings['base_url']);
        $this->setRedirectUrl($instagram_settings['redirect_url']);
        $this->setBaseAccessToken($instagram_settings['base_access_token']);
        $this->CurlService = $CurlService;        
        $this->username = null;
        $this->full_name = null;
        $this->profile = null;
    }

    public function setBaseAccessToken($token)
    {
        $this->base_access_token = $token;        
    }

    public function getBaseAccessToken()
    {        
        return $this->base_access_token;
    }

    public function setRedirectUrl($redirect_url)
    {
        $this->redirect_url = $redirect_url;
    }

    public function getRedirectUrl()
    {
        return $this->redirect_url;
    }

    public function getAuthenticationUrl()
    {
        $uri = $this->api_url 
                . '/oauth/authorize/?client_id=' 
                . $this->client_id 
                . '&redirect_uri=' 
                . $this->redirect_url . '&response_type=code'
                . '&scope=basic+public_content'
        ;
        return $uri;
    }

    public function setUserAccessToken($token)
    {
        $this->user_access_token = $token;         
    }

    public function getUserAccessToken()
    {
        return $this->user_access_token;
    }

    public function searchForUser($term = null, $access_token = null)
    {
        $userurl = '/v1/users/search?q=' . urlencode($term);
        $users = $this->apiCall($userurl, $access_token);
 
        return $users;
    }

    public function searchForTag($term = null, $access_token = null)
    {
        $url = '/v1/tags/search?q=' . urlencode($term);
        $tags = $this->apiCall($url, $access_token);
 
        return $tags;
    }

    public function searchForTagMedia($tag = null, $access_token = null)
    {
        $url = '/v1/tags/' . urlencode($tag) . '/media/recent?';
        $media = $this->apiCall($url, $access_token);
 
        return $media;
    }

    public function searchForUserMedia($user = null, $access_token = null)
    {
        $url = '/v1/users/' . urlencode($user) . '/media/recent?';
        $media = $this->apiCall($url, $access_token);
        return $media;
    }

    public function getUserInfo($user = null, $access_token = null)
    {
        $url = '/v1/users/' . urlencode($user) . '?';
        $user = $this->apiCall($url, $access_token);
        return $user;
    }


    public function getClientSecret()
    {
        return $this->client_secret;
    }

    public function setClientSecret($client_secret)
    {
        $this->client_secret = $client_secret;       
    }

    public function getClientId()
    {
        return $this->client_id;
    }

    public function setClientId($client_id)
    {
        $this->client_id = $client_id;       
    }

    public function getApiUrl()
    {
        return $this->api_url;
    }

    public function setApiUrl($api_url)
    {
        $this->api_url = $api_url;     
    }

    public function requestAccessTokenFromAuth($auth_code)
    {
        $uri = "/oauth/access_token";
        $data = [
            'client_secret'=>$this->client_secret
            ,'grant_type' => 'authorization_code'
            ,'redirect_uri'=> $this->redirect_url
            ,'code' => $auth_code
            ,'client_id' => $this->client_id
        ];

        $result = $this->apiCall($uri, $data);

        if(empty($result['access_token']))
            throw new \Exception('Failed to obtain access token.');        
      
        return $result;
    }


    public function prepareUserResults($user_results)
    {
        $new_results = [];
        if(!empty($user_results) && is_array($user_results) && !empty($user_results['data']))
        {
            foreach($user_results['data'] as $user)
            {
                $new_results[$user['id']] = $user;
            }
        }     

        return $new_results;
    }

    public function apiCall($uri, $access_token = null)
    {
        $url = $this->api_url . $uri;
        if(!empty($access_token))
        {
            if(!is_array($access_token))
                $url .= '&access_token=' . $access_token;
            else            
                $this->CurlService->setPostData($access_token);            
        }

        return json_decode($this->CurlService->processUrl($url), true);
    }

}

?>
